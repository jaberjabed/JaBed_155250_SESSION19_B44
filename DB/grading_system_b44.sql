-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2017 at 07:17 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `grading_system_b44`
--

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
`id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `roll` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `mark_bangla` float NOT NULL,
  `mark_english` float NOT NULL,
  `mark_math` float NOT NULL,
  `grade_bangla` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `grade_english` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `grade_math` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `name`, `roll`, `mark_bangla`, `mark_english`, `mark_math`, `grade_bangla`, `grade_english`, `grade_math`) VALUES
(1, 'Shahadat', '33', 67, 55, 98, 'B+', 'B-', 'A+'),
(2, 'Shahadat', '33', 67, 77, 55, 'B+', 'A', 'B-'),
(3, 'Shahadat', '34', 54, 76, 22, 'C+', 'A', 'F'),
(5, 'ee', '33', 45, 67, 78, 'C', 'B+', 'A'),
(6, 'jf', '22', 33, 12, 44, 'F', 'F', 'D'),
(7, 'hello', '12', 34, 56, 67, 'F', 'B-', 'B+'),
(8, 'Roki', '12', 33, 45, 67, 'F', 'C', 'B+');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `result`
--
ALTER TABLE `result`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
