<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 11:12 AM
 */

namespace App;

use PDO, PDOException;


class Database
{

    public $DBH;



    public function __construct()
    {
        if(!isset($_SESSION))session_start();

        try{

            $this->DBH = new PDO('mysql:host=localhost;dbname=grading_system_b44', 'root', '');

            echo "Database connection successful!<br>";

        }
        catch(PDOException $error){

            echo "Database Error: ". $error->getMessage();
        }
    }

}