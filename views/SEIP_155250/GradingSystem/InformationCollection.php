<?php

    require_once("../../../vendor/autoload.php");

    if(!isset($_SESSION))session_start();

    $msg = \App\Message::message();

    echo "<div id = 'message'> $msg </div> <br><br>";

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Collection Form</title>
</head>
<body>

<form action="process.php" method="post">


    Enter Student's Name:<br>
    <input type="text" name="name"><br><br>

    Enter Student's Roll:<br>
    <input type="number" name="roll"><br><br><br>

    Bangla:<br>
    <input type="number" name="markBangla" step="any" min="0" max="100"><br><br>

     English:<br>
    <input type="number" name="markEnglish" step="any" min="0" max="100"><br><br>

     Math:<br>
    <input type="number" name="markMath" step="any" min="0" max="100"><br><br>

    <input type="submit">




</form>




<script src = "../../../resource/bootstrap/js/jquery.js"></script>

<script>

    jQuery(

        function($){

            $('#message').fadeOut(5500);
            $('#message').fadeIn(5500);
        }
    )
</script>


</body>
</html>