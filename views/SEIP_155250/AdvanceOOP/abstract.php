<?php


abstract class MyAbsClass{

    public $name;

    abstract protected function forcedMethod1();
    abstract protected function forcedMethod2($price);

    public function doSomething(){

        echo "I am doing everything !!<br>";
    }
}

class MySimpleClass extends MyAbsClass{


    private $id;
    public $result=40;


    public function forcedMethod1()
    {
       echo "I am inside ".__METHOD__."<br>"; // TODO: Implement forcedMethod1() method.
    }

    public function forcedMethod2($price)
    {
        echo "I am inside ".__METHOD__."<br>";// TODO: Implement forcedMethod2() method.
    }

    public function simpleMethod1(){

        echo $this->result;
    }
}

