<?php
trait Hello {
    public function sayHello() {
        echo 'Hello ';
    }
}

trait World {
    public function sayWorld() {
        echo 'World';
    }
}


trait BigHello {
    public function sayyHello() {
        echo 'HELLO ';
    }
}

trait SmallHello {
    public function sayyHello() {
        echo 'WORLD';
    }
}


class HelloWorld {
    use Hello, World;

    use BigHello, SmallHello{
        BigHello::sayyHello insteadof SmallHello;
        SmallHello::sayyHello as tellHello;
    }




    public function sayHelloWorld(){

        $this->sayHello();
        $this->sayWorld();

        echo "<br>";

        $this->sayyHello();
        $this->tellHello();


    }
    public function sayExclamationMark() {
        echo " !!<br>";
    }
}

$obj = new HelloWorld();

$obj->sayHelloWorld();
$obj->sayExclamationMark();
