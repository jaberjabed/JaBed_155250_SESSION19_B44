<?php


class Bird{

    public $name = "just bird";
    public function info(){

        echo "I am $this->name <br>";

        echo "I am a bird";
    }
}

interface canFly{


    public function fly();
}

interface canSwim{


    public function swim();
}


class Dove extends Bird implements canFly{

        public $name = "Dove !";
        public function fly()
        {
            echo "<br>I can fly<br>";
        }
}

class Penguine extends Bird implements canSwim{

    public $name = "Penguine !";
    public function swim()
    {
        echo "<br>I can swim<br>";
    }
}

class Duck extends Bird implements canFly, canSwim{

    public $name = "Duck !";
    public function fly()
    {
        echo "<br>I can fly<br>";
    }

    public function swim()
    {
        echo "I can swim<br>";
    }

}


function describe($bird){

    $bird ->info();

    if($bird instanceof canFly){

        $bird->fly();
    }
    if($bird instanceof canSwim){

        $bird->swim();
    }

}


describe(new Bird());
echo "<br><br>";
describe(new Dove());
echo "<br><br>";

describe(new Penguine());
echo "<br><br>";
describe(new Duck());